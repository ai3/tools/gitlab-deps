import logging


def check_hook(gl, hook_url, webhook_token, project_path, dry_run):
    project = gl.projects.get(project_path)
    found = False
    for h in project.hooks.list():
        if h.url == hook_url and h.pipeline_events:
            if not dry_run:
                h.token = webhook_token
                h.save()
            found = True
            break
    if found:
        return
    logging.info('adding pipeline_events hook to %s', project_path)
    if not dry_run:
        project.hooks.create({
            'url': hook_url,
            'push_events': False,
            'pipeline_events': True,
            'token': webhook_token,
        })


def clear_hooks(gl, project_path, dry_run):
    project = gl.projects.get(project_path)
    for h in project.hooks.list():
        logging.info('%s: removing hook %s', project_path, h.url)
        if not dry_run:
            h.delete()

