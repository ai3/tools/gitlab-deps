#!/bin/bash

deps_file=/var/lib/gitlab-deps/deps.list
rev_deps_file=/var/lib/gitlab-deps/reverse-deps.list
ret=0

[ -e /etc/default/gitlab-deps ] && . /etc/default/gitlab-deps

if [ -z "$GITLAB_URL" -o -z "$GITLAB_TOKEN_FILE" ]; then
    echo "Gitlab-deps is not configured" >&2
    exit 2
fi

opts="--token-file=$GITLAB_TOKEN_FILE --url=$GITLAB_URL"

tmp_file="${deps_file}.tmp.$$"
trap "rm -f $tmp_file 2>/dev/null" EXIT

set -o pipefail
gitlab-deps list-projects $opts \
    | egrep "${PROJECT_REGEXP:-.*}" \
    | gitlab-deps deps $opts \
        ${GITLAB_REGISTRY_HOSTNAME:+--registry=${GITLAB_REGISTRY_HOSTNAME}} \
        > $tmp_file
if [ $? -gt 0 ]; then
    rm -f $tmp_file
    exit 1
fi

mv -f $tmp_file $deps_file
awk '{print $2, $1}' < $deps_file > $rev_deps_file

exit 0
